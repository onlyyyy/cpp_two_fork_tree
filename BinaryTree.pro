TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        BTree.cpp \
        main.cpp

HEADERS += \
    BTree.h
